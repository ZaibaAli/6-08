//Pranathi Mothe
const mongoose = require('mongoose')

const OrderLineItemsSchema = new mongoose.Schema({

  _id: { type: Number, 
    required: true 
  },
  order_id: {
    type: Number,
    required: true,
    default: 0
  },
  product_id: {
    type: Number,
    required: true,
    default: 0
  },
  lineNo: {
    type: Number,
    required: true,
    default: 1,
  },
  orderQuantity: {
    type: Number,
    required: true,
    default: 0,
    min: 0,
    max: 100000
  }
})
module.exports = mongoose.model('OrderLineItems', OrderLineItemsSchema)
